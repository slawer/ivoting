def login
  request.body.rewind
  payload = JSON.parse(request.body.read)
  username = payload['email']
  password = payload['password']

  if User.exists?(:email => username)
    #user = User.find_by_user_name(username)

    user = User.where(email: username).select("id,email,username,last_name,encrypted_password,password_hash,password_salt,other_names,contact_number,polling_station_id,role_id").first

    puts "#{user.inspect}"

    if user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      _info = PollingStationMaster.joins("left join users ON users.polling_station_id = polling_station_masters.id").select("polling_station_masters.id,name,constituency_master_id").where(id: user.polling_station_id).first

      _const_name = ConstituencyMaster.select("id,name").where(id: _info.constituency_master_id).first

      puts "#{_info.inspect}"
      puts "#{_const_name.inspect}"
      puts "#{_const_name.name}"
      hash = {
          "last_name" => "#{user.last_name}",
          "other_names" => "#{user.other_names}",
          "username" => "#{user.username}",
          "user_id" => "#{user.id}",
          "email" => "#{user.email}",
          "contact_number" => "#{user.contact_number}",
          "polling_station_id" => "#{user.polling_station_id}",
          "polling_station_name" => "#{_info.name}",
          "constituency_id" => "#{_const_name.id}",
          "constituency_name" => "#{_const_name.name}"
      }

      _hash = {
          "resp_code" => "000",
          "resp_desc" => "Success",
          "message" => hash
      }
      puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
      puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
      puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
      puts ">>>>>>>>>>>>>>>>>>>> #{_hash.inspect}>>>>>>>>>>>>"
      Oj.dump(_hash, :pretty => true)
    else


      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "********                                          ***********"
      puts "********             USER AUTH FAILED             ***********"
      puts "********          #{ERR_SIGN_IN.inspect}          ***********"
      puts "********                                          ***********"
      puts "********                                          ***********"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      halt Oj.dump(ERR_SIGN_IN, :pretty => true)
    end
  else


    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "********                                          ***********"
    puts "********             USER NOT FOUND               ***********"
    puts "********          #{ERR_EXIST.inspect}            ***********"
    puts "********                                          ***********"
    puts "********                                          ***********"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    halt Oj.dump(ERR_EXIST, :pretty => true)
  end

end

def createAccount
  request.body.rewind
  payload = JSON.parse(request.body.read)
  email = payload["email"]
  last_name = payload["last_name"]
  other_names = payload["other_names"]
  contact = payload["phone_number"]
  password = payload["password"]


  # if checkUserExistence(email)

  if User.exists?(:email => email)
    #user = User.find_by_user_name(username)

    user = User.where(email: email).select("id,email,username,last_name,encrypted_password,password_hash,password_salt,other_names,contact_number,polling_station_id,role_id").first

    puts "#{user.inspect}"

    password_salt = BCrypt::Engine.generate_salt
    password_hash = BCrypt::Engine.hash_secret(password, password_salt)

    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "********                                          ***********"
    puts "********                                          ***********"
    puts "********           USER ACCOUNT CREATION          ***********"
    puts "********                                          ***********"
    puts "********                                          ***********"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"

    createAppUser(last_name, other_names, email, contact, password_hash, password_salt)


  else

    str = {
        "resp_code" => "050",
        "resp_desc" => "Failure",
        "message" => "You are not allowed to use this service."
    }

    user_data = {
        "email" => email,
        "last_name" => last_name,
        "other_names" => other_names,
        "contact" => contact,
        "password" => password
    }

    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "********                                          ***********"
    puts "********             USER NOT FOUND               ***********"
    puts "********            #{str.inspect}                ***********"
    puts "********          #{user_data.inspect}            ***********"
    puts "********                                          ***********"
    puts "********                                          ***********"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"


    halt Oj.dump(str, :pretty => true)
  end

end

def getVoteMaster
  _vote_type = VoteMaster.where(status: true).order('id asc')

  if _vote_type.present?

    _result = ({'resp_desc' => "Success", 'resp_code' => "010", 'message' => _vote_type})


    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>> #{_result.inspect}>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    MultiJson.dump(_result, :pretty => true)
  else
    halt Oj.dump(ERR_NO_UTYPE, :pretty => true)
  end
end

def addBookletDets
  request.body.rewind
  payload = JSON.parse(request.body.read)

  user_id = payload['user_id']
  polling_station_id = payload['polling_station_id']
  vote_master_id = payload['vote_master_id']
  registed_voters = payload['registered_voters']
  booklet_number = payload['booklet_number']
  ballot_papers_total = payload['ballot_papers_total']

  _book_data = BookletMaster.where(user_id: user_id, vote_master_id: vote_master_id).order('id desc').first

  if _book_data.present?
    _already_exist = {
        "resp_code" => "021",
        "resp_desc" => "Failure",
        "message" => "Cannot make any form of changes"
    }

    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>> #{_already_exist.inspect}>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    halt Oj.dump(_already_exist, :pretty => true)
  else

    createBooklet(user_id, polling_station_id, vote_master_id, registed_voters, booklet_number, ballot_papers_total)

  end
end

def getVoteInventory
  request.body.rewind

  payload = JSON.parse(request.body.read)

  user_id = payload["user_id"]
  polling_station_id = payload["polling_station_id"]

  _booklet_info = BookletMaster.where(user_id: user_id).select("user_id, polling_station_master_id , vote_master_id, registed_voters, booklet_number, ballot_papers_total").order('id asc') #.as_json(:except => :id)

  puts "============================================#{_booklet_info.inspect}====================================================="

  if _booklet_info.present?
    _polling_station_info = PollingStationMaster.joins('left join booklet_masters ON booklet_masters.polling_station_master_id = polling_station_masters.id').select("polling_station_masters.id,name").where("polling_station_masters.id = #{polling_station_id}")[0]
    puts "============================================#{_polling_station_info.inspect}====================================================="

    _vote_master_info = VoteMaster.joins('left join booklet_masters ON booklet_masters.vote_master_id = vote_masters.id').select("vote_masters.id,name")
    puts "============================================#{_vote_master_info.inspect}====================================================="


    _arr_schedules = []
    vote_type_name = ""
    _booklet_info.each do |booklet|
      _bio_data_hash = {}
      _bio_data_hash["user_id"] = booklet.user_id
      _bio_data_hash["polling_station_master_id"] = booklet.polling_station_master_id
      _bio_data_hash["polling_station_name"] = _polling_station_info.name
      _bio_data_hash["vote_master_id"] = booklet.vote_master_id

      _vote_type_arr = _vote_master_info.where("vote_masters.id = #{booklet.vote_master_id}")

      puts "The vote type name before:::::#{vote_type_name}"
      _vote_type_arr.each do |vote|
        vote_type_name = vote.name
      end
      _bio_data_hash["vote_master_name"] = vote_type_name
      _bio_data_hash["registed_voters"] = booklet.registed_voters
      _bio_data_hash["booklet_number"] = booklet.booklet_number
      _bio_data_hash["ballot_papers_total"] = booklet.ballot_papers_total

      puts "The vote type name after:::::#{vote_type_name}"

      _arr_schedules << _bio_data_hash

    end
    _final_resp = {
        "resp_code" => "000",
        "resp_desc" => "Success",
        "message" => _arr_schedules,
    }
    puts "============================================#{_final_resp.inspect}====================================================="
    MultiJson.dump(_final_resp, :pretty => true)
  else
    _final_resp = {
        "resp_code" => "001",
        "resp_desc" => "Success",
        "message" => "No Record Found",
    }
    puts "============================================#{_final_resp.inspect}====================================================="
    MultiJson.dump(_final_resp, :pretty => true)
  end

end

def getVotingResults
  request.body.rewind

  payload = JSON.parse(request.body.read)

  #vote_type_id = payload["vote_type_id"]
  _user_id = payload["user_id"]
  polling_station_id = payload["polling_station_id"]


  _arr_candidates = []
  _pres_arr_candidates = []
  vote_type_name = ""
  const_name = ""
  party_name = ""

  _record_info = RecordsMaster.where(user_id: _user_id, polling_station_id: polling_station_id).select('pink_sheet_master_id,
      candidate_master_id,
      polling_station_id,
      votes_obtained,
      user_id,
      created_at,
      updated_at,
      presidential_vote_status,
      parliamentary_vote_status')

  _record_info.each do |record|

    _cadi = CandidateMaster.where(id: record.candidate_master_id)


    puts "**********************************#{_cadi.inspect}*************************************************"
    # puts "**********************************#{_cadi.name}*************************************************"
    # puts "**********************************#{_cadi.party_master_id}*************************************************"

    _cadi.each do |cadi|
      if cadi.vote_master_id == 1
        _record_result_obj = {}
        _record_result_obj["pink_sheet_master_id"] = record.pink_sheet_master_id
        _record_result_obj["polling_station_id"] = record.polling_station_id
        _record_result_obj["candidate_master_id"] = record.candidate_master_id
        _record_result_obj["votes_obtained"] = record.votes_obtained

        _arr_candidates << _record_result_obj
      elsif cadi.vote_master_id == 2
        _pres_record_result_obj = {}
        _pres_record_result_obj["pink_sheet_master_id"] = record.pink_sheet_master_id
        _pres_record_result_obj["polling_station_id"] = record.polling_station_id
        _pres_record_result_obj["candidate_master_id"] = record.candidate_master_id
        _pres_record_result_obj["votes_obtained"] = record.votes_obtained

        _pres_arr_candidates << _pres_record_result_obj

      end
    end
  end
  # _candidate_info = CandidateMaster.where(vote_master_id: vote_type_id).select('constituency_master_id')
  # _vote_master_info = VoteMaster.joins('left join candidate_masters ON candidate_masters.vote_master_id = vote_masters.id').select("vote_masters.id,vote_masters.name")
  # _constituency_master_info = ConstituencyMaster.joins('left join candidate_masters ON candidate_masters.constituency_master_id = constituency_masters.id').select("constituency_masters.id,constituency_masters.name")
  # _party_master_info = PartyMaster.joins('left join candidate_masters ON candidate_masters.party_master_id = party_masters.id').select("party_masters.id,party_masters.name")
  puts "\n============================================#{_record_info.inspect}====================================================="


  # check_result_pal_existence = RecordsMaster.where(polling_station_id: polling_station_id, user_id: _user_id, parliamentary_vote_status: true, presidential_vote_status: false)
  # check_result_presi_existence = RecordsMaster.where(polling_station_id: polling_station_id, user_id: _user_id, parliamentary_vote_status: false, presidential_vote_status: true)
  #
  # vote_master = VoteMaster.where(id: vote_type_id).first
  #
  # puts "\n*********** Vote Master #{vote_master.inspect} #{vote_master.id} ***************"
  # puts "\n*********** Pal Record #{check_result_pal_existence.inspect} ***************"
  # puts "\n*********** Pres Record #{check_result_presi_existence.inspect} ***************"
  _check_cand_id = ""


  # if vote_master.id == 1
  #   if _check_cand_id
  #
  #     if check_result_pal_existence.exists?
  #       puts "*************************************************************"
  #       puts "*************************************************************"
  #       puts "*********************You have already done parliamentary***********************"
  #       puts "*************************************************************"
  #       puts "*************************************************************"
  #
  #       _exist = {
  #           "resp_code" => "024",
  #           "resp_desc" => "Exists",
  #           "message" => "You has uploaded parliamentary results"
  #       }
  #       halt Oj.dump(_exist, :pretty => true)
  #     else
  #       _pal_candi = CandidateMaster.where(vote_master_id: vote_type_id, constituency_master_id: _check_cand_id)
  #
  #       _pal_candi.each do |pal_candi|
  #         _candidates_hash = {}
  #         #_candidates_hash["user_id"] = pal_candi.user_id
  #         _candidates_hash["candidate_name"] = pal_candi.name
  #         _candidates_hash["candidate_id"] = pal_candi.id
  #         _candidates_hash["polling_station_name"] = pal_candi.name
  #         _candidates_hash["constituency_master_id"] = _check_cand_id
  #         _candidates_hash["vote_master_id"] = pal_candi.vote_master_id
  #
  #         _vote_type_arr = _vote_master_info.where("vote_masters.id = #{pal_candi.vote_master_id}")
  #         _const_master_arr = _constituency_master_info.where("constituency_masters.id = #{_check_cand_id}")
  #         _party_master_arr = _party_master_info.where("party_masters.id = #{pal_candi.party_master_id}")
  #
  #         puts "The vote type name before:::::#{vote_type_name}"
  #         puts "The const name before:::::#{const_name}"
  #         puts "The party name before:::::#{party_name}"
  #         _vote_type_arr.each do |vote|
  #           vote_type_name = vote.name
  #         end
  #
  #         _const_master_arr.each do |const|
  #           const_name = const.name
  #         end
  #
  #         _party_master_arr.each do |party|
  #           party_name = party.name
  #         end
  #
  #         _candidates_hash["vote_master_name"] = vote_type_name
  #         _candidates_hash["constituency_name"] = const_name
  #         _candidates_hash["candidate_position"] = pal_candi.position
  #         _candidates_hash["party_name"] = party_name
  #         _candidates_hash["party_master_id"] = pal_candi.party_master_id
  #         _candidates_hash["file_url"] = pal_candi.file_url
  #
  #         puts "The vote type name after:::::#{vote_type_name}"
  #         puts "The const name after:::::#{const_name}"
  #
  #         _arr_candidates << _candidates_hash
  #
  #       end
  #
  #       puts "=========================#{_pal_candi.inspect}======================="
  #
  #       _final_resp = {
  #           "resp_code" => "000",
  #           "resp_desc" => "Success",
  #           "message" => _arr_candidates,
  #       }
  #       MultiJson.dump(_final_resp, :pretty => true)
  #     end
  #   end
  # elsif vote_master.id == 2
  #   if check_result_presi_existence.exists?
  #     puts "*************************************************************"
  #     puts "*************************************************************"
  #     puts "*********************You have already done presidential***********************"
  #     puts "*************************************************************"
  #     puts "*************************************************************"
  #
  #     _exist = {
  #         "resp_code" => "025",
  #         "resp_desc" => "Exists",
  #         "message" => "You has uploaded presidential results"
  #     }
  #     halt Oj.dump(_exist, :pretty => true)
  #   else
  #     _pres_candi = CandidateMaster.where(vote_master_id: vote_type_id, constituency_master_id: _check_cand_id)
  #     puts "=========================#{_pres_candi.inspect}======================="
  #
  #     _pres_candi.each do |pres_candi|
  #       _candidates_hash = {}
  #       _candidates_hash["candidate_name"] = pres_candi.name
  #       _candidates_hash["candidate_id"] = pres_candi.id
  #       _candidates_hash["vote_master_id"] = pres_candi.vote_master_id
  #       _candidates_hash["candidate_position"] = pres_candi.position
  #       _candidates_hash["constituency_master_id"] = _check_cand_id
  #       _candidates_hash["party_master_id"] = pres_candi.party_master_id
  #       _candidates_hash["file_url"] = pres_candi.file_url
  #
  #       # _candidates_hash["polling_station_name"] = _polling_station_info.name
  #       # _candidates_hash["vote_master_id"] = booklet.vote_master_id
  #
  #       _vote_type_arr = _vote_master_info.where("vote_masters.id = #{pres_candi.vote_master_id}")
  #       if _check_cand_id == nil?
  #
  #         _check_cand_id = nil
  #         _const_master_arr = _constituency_master_info.where("constituency_masters.id = #{_check_cand_id}")
  #
  #         _const_master_arr.each do |const|
  #           const_name = const.name
  #         end
  #       end
  #       _party_master_arr = _party_master_info.where("party_masters.id = #{pres_candi.party_master_id}")
  #
  #
  #       puts "The const name before:::::#{const_name}"
  #
  #       puts "The file path:::::#{pres_candi.file_url}"
  #       _vote_type_arr.each do |vote|
  #         vote_type_name = vote.name
  #       end
  #
  #
  #       _party_master_arr.each do |party|
  #         party_name = party.name
  #       end
  #
  #       _candidates_hash["vote_master_name"] = vote_type_name
  #       _candidates_hash["constituency_name"] = const_name
  #       _candidates_hash["party_name"] = party_name
  #       # _candidates_hash["registed_voters"] = booklet.registed_voters
  #       # _candidates_hash["booklet_number"] = booklet.booklet_number
  #       # _candidates_hash["ballot_papers_total"] = booklet.ballot_papers_total
  #
  #       puts "The vote type name after:::::#{vote_type_name}"
  #
  #       _arr_candidates << _candidates_hash
  #
  #     end
  _final_resp = {
      "resp_code" => "000",
      "resp_desc" => "Success",
      "parliamentary" => _arr_candidates,
      "presi" => _pres_arr_candidates,
  }
  MultiJson.dump(_final_resp, :pretty => true)
  #   end
  # end

end

def getCandidates
  request.body.rewind

  payload = JSON.parse(request.body.read)

  vote_type_id = payload["vote_type_id"]
  _user_id = payload["user_id"]
  polling_station_id = payload["polling_station_id"]

  _arr_candidates = []
  vote_type_name = ""
  const_name = ""
  party_name = ""
  party_image_url = ""

  _candidate_info = CandidateMaster.where(vote_master_id: vote_type_id).select('constituency_master_id')
  _vote_master_info = VoteMaster.joins('left join candidate_masters ON candidate_masters.vote_master_id = vote_masters.id').select("vote_masters.id,vote_masters.name")
  _constituency_master_info = ConstituencyMaster.joins('left join candidate_masters ON candidate_masters.constituency_master_id = constituency_masters.id').select("constituency_masters.id,constituency_masters.name")
  _party_master_info = PartyMaster.joins('left join candidate_masters ON candidate_masters.party_master_id = party_masters.id').select("party_masters.id,party_masters.name,party_masters.party_image_url")
  puts "\n============================================#{_vote_master_info.inspect}====================================================="
  puts "\n============================================#{_constituency_master_info.inspect}====================================================="
  puts "\n============================================#{_party_master_info.inspect}====================================================="


  check_result_pal_existence = RecordsMaster.where(polling_station_id: polling_station_id, user_id: _user_id, parliamentary_vote_status: true, presidential_vote_status: false)
  check_result_presi_existence = RecordsMaster.where(polling_station_id: polling_station_id, user_id: _user_id, parliamentary_vote_status: false, presidential_vote_status: true)

  vote_master = VoteMaster.where(id: vote_type_id).first

  puts "\n*********** Vote Master #{vote_master.inspect} #{vote_master.id} ***************"
  puts "\n*********** Pal Record #{check_result_pal_existence.inspect} ***************"
  puts "\n*********** Pres Record #{check_result_presi_existence.inspect} ***************"
  _check_cand_id = ""

  _candidate_info.each do |candidate|

    _check_cand_id = candidate.constituency_master_id

  end

  if vote_master.id == 1
    if _check_cand_id

      if check_result_pal_existence.exists?
        puts "*************************************************************"
        puts "*************************************************************"
        puts "*********************You have already done parliamentary***********************"
        puts "*************************************************************"
        puts "*************************************************************"

        _exist = {
            "resp_code" => "024",
            "resp_desc" => "Exists",
            "message" => "You has uploaded parliamentary results"
        }
        halt Oj.dump(_exist, :pretty => true)
      else
        _pal_candi = CandidateMaster.where(vote_master_id: vote_type_id, constituency_master_id: _check_cand_id)

        _pal_candi.each do |pal_candi|
          _candidates_hash = {}
          #_candidates_hash["user_id"] = pal_candi.user_id
          _candidates_hash["candidate_name"] = pal_candi.name
          _candidates_hash["candidate_id"] = pal_candi.id
          _candidates_hash["polling_station_name"] = pal_candi.name
          _candidates_hash["constituency_master_id"] = _check_cand_id
          _candidates_hash["vote_master_id"] = pal_candi.vote_master_id

          _vote_type_arr = _vote_master_info.where("vote_masters.id = #{pal_candi.vote_master_id}")
          _const_master_arr = _constituency_master_info.where("constituency_masters.id = #{_check_cand_id}")
          _party_master_arr = _party_master_info.where("party_masters.id = #{pal_candi.party_master_id}")

          puts "The vote type name before:::::#{vote_type_name}"
          puts "The const name before:::::#{const_name}"
          puts "The party name before:::::#{party_name}"
          _vote_type_arr.each do |vote|
            vote_type_name = vote.name
          end

          _const_master_arr.each do |const|
            const_name = const.name
          end

          _party_master_arr.each do |party|
            party_name = party.name
            party_image_url = party.party_image_url
          end

          _candidates_hash["vote_master_name"] = vote_type_name
          _candidates_hash["constituency_name"] = const_name
          _candidates_hash["candidate_position"] = pal_candi.position
          _candidates_hash["party_name"] = party_name
          _candidates_hash["party_image_url"] = party_image_url
          _candidates_hash["party_master_id"] = pal_candi.party_master_id
          _candidates_hash["file_url"] = pal_candi.file_url

          puts "The vote type name after:::::#{vote_type_name}"
          puts "The const name after:::::#{const_name}"

          _arr_candidates << _candidates_hash

        end

        puts "=========================#{_pal_candi.inspect}======================="

        _final_resp = {
            "resp_code" => "000",
            "resp_desc" => "Success",
            "message" => _arr_candidates,
        }
        MultiJson.dump(_final_resp, :pretty => true)
      end
    end
  elsif vote_master.id == 2
    if check_result_presi_existence.exists?
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*********************You have already done presidential***********************"
      puts "*************************************************************"
      puts "*************************************************************"

      _exist = {
          "resp_code" => "025",
          "resp_desc" => "Exists",
          "message" => "You has uploaded presidential results"
      }
      halt Oj.dump(_exist, :pretty => true)
    else
      _pres_candi = CandidateMaster.where(vote_master_id: vote_type_id, constituency_master_id: _check_cand_id)
      puts "=========================#{_pres_candi.inspect}======================="

      _pres_candi.each do |pres_candi|
        _candidates_hash = {}
        _candidates_hash["candidate_name"] = pres_candi.name
        _candidates_hash["candidate_id"] = pres_candi.id
        _candidates_hash["vote_master_id"] = pres_candi.vote_master_id
        _candidates_hash["candidate_position"] = pres_candi.position
        _candidates_hash["constituency_master_id"] = _check_cand_id
        _candidates_hash["party_master_id"] = pres_candi.party_master_id
        _candidates_hash["file_url"] = pres_candi.file_url

        # _candidates_hash["polling_station_name"] = _polling_station_info.name
        # _candidates_hash["vote_master_id"] = booklet.vote_master_id

        _vote_type_arr = _vote_master_info.where("vote_masters.id = #{pres_candi.vote_master_id}")
        if _check_cand_id == nil?

          _check_cand_id = nil
          _const_master_arr = _constituency_master_info.where("constituency_masters.id = #{_check_cand_id}")

          _const_master_arr.each do |const|
            const_name = const.name
          end
        end
        _party_master_arr = _party_master_info.where("party_masters.id = #{pres_candi.party_master_id}")


        puts "The const name before:::::#{const_name}"

        puts "The file path:::::#{pres_candi.file_url}"
        _vote_type_arr.each do |vote|
          vote_type_name = vote.name
        end


        _party_master_arr.each do |party|
          party_name = party.name
          party_image_url = party.party_image_url
        end

        _candidates_hash["vote_master_name"] = vote_type_name
        _candidates_hash["constituency_name"] = const_name
        _candidates_hash["party_name"] = party_name
        _candidates_hash["party_image_url"] = party_image_url
        # _candidates_hash["registed_voters"] = booklet.registed_voters
        # _candidates_hash["booklet_number"] = booklet.booklet_number
        # _candidates_hash["ballot_papers_total"] = booklet.ballot_papers_total

        puts "The vote type name after:::::#{vote_type_name}"

        _arr_candidates << _candidates_hash

      end
      _final_resp = {
          "resp_code" => "000",
          "resp_desc" => "Success",
          "message" => _arr_candidates,
      }
      MultiJson.dump(_final_resp, :pretty => true)
    end
  end

end

# def getCandidates
#   request.body.rewind
#
#   payload = JSON.parse(request.body.read)
#
#   vote_type_id = payload["vote_type_id"]
#   _user_id = payload["user_id"]
#   polling_station_id = payload["polling_station_id"]
#
#   _arr_candidates = []
#   vote_type_name = ""
#   const_name = ""
#   party_name = ""
#
#   _candidate_info = CandidateMaster.where(vote_master_id: vote_type_id).select('constituency_master_id')
#   _vote_master_info = VoteMaster.joins('left join candidate_masters ON candidate_masters.vote_master_id = vote_masters.id').select("vote_masters.id,vote_masters.name")
#   _constituency_master_info = ConstituencyMaster.joins('left join candidate_masters ON candidate_masters.constituency_master_id = constituency_masters.id').select("constituency_masters.id,constituency_masters.name")
#   _party_master_info = PartyMaster.joins('left join candidate_masters ON candidate_masters.party_master_id = party_masters.id').select("party_masters.id,party_masters.name")
#   puts "\n============================================#{_vote_master_info.inspect}====================================================="
#   puts "\n============================================#{_constituency_master_info.inspect}====================================================="
#   puts "\n============================================#{_party_master_info.inspect}====================================================="
#
#
#   check_result_pal_existence = RecordsMaster.where(polling_station_id: polling_station_id, user_id: _user_id, parliamentary_vote_status: true, presidential_vote_status: false)
#   check_result_presi_existence = RecordsMaster.where(polling_station_id: polling_station_id, user_id: _user_id, parliamentary_vote_status: false, presidential_vote_status: true)
#
#   vote_master = VoteMaster.where(id: vote_type_id).first
#
#   puts "\n*********** Vote Master #{vote_master.inspect} #{vote_master.id} ***************"
#   puts "\n*********** Pal Record #{check_result_pal_existence.inspect} ***************"
#   puts "\n*********** Pres Record #{check_result_presi_existence.inspect} ***************"
#   _check_cand_id = ""
#
#   _candidate_info.each do |candidate|
#
#     _check_cand_id = candidate.constituency_master_id
#
#   end
#
#   if vote_master.id == 1
#     if _check_cand_id
#
#       # if check_result_pal_existence.exists?
#       #   puts "*************************************************************"
#       #   puts "*************************************************************"
#       #   puts "*********************You have already done parliamentary***********************"
#       #   puts "*************************************************************"
#       #   puts "*************************************************************"
#       #
#       #   _exist = {
#       #       "resp_code" => "024",
#       #       "resp_desc" => "Exists",
#       #       "message" => "You has uploaded parliamentary results"
#       #   }
#       #   halt Oj.dump(_exist, :pretty => true)
#       # else
#         _pal_candi = CandidateMaster.where(vote_master_id: vote_type_id, constituency_master_id: _check_cand_id)
#
#         _pal_candi.each do |pal_candi|
#           _candidates_hash = {}
#           #_candidates_hash["user_id"] = pal_candi.user_id
#           _candidates_hash["candidate_name"] = pal_candi.name
#           _candidates_hash["candidate_id"] = pal_candi.id
#           _candidates_hash["polling_station_name"] = pal_candi.name
#           _candidates_hash["constituency_master_id"] = _check_cand_id
#           _candidates_hash["vote_master_id"] = pal_candi.vote_master_id
#
#           _vote_type_arr = _vote_master_info.where("vote_masters.id = #{pal_candi.vote_master_id}")
#           _const_master_arr = _constituency_master_info.where("constituency_masters.id = #{_check_cand_id}")
#           _party_master_arr = _party_master_info.where("party_masters.id = #{pal_candi.party_master_id}")
#
#           puts "The vote type name before:::::#{vote_type_name}"
#           puts "The const name before:::::#{const_name}"
#           puts "The party name before:::::#{party_name}"
#           _vote_type_arr.each do |vote|
#             vote_type_name = vote.name
#           end
#
#           _const_master_arr.each do |const|
#             const_name = const.name
#           end
#
#           _party_master_arr.each do |party|
#             party_name = party.name
#           end
#
#           _candidates_hash["vote_master_name"] = vote_type_name
#           _candidates_hash["constituency_name"] = const_name
#           _candidates_hash["candidate_position"] = pal_candi.position
#           _candidates_hash["party_name"] = party_name
#           _candidates_hash["party_master_id"] = pal_candi.party_master_id
#           _candidates_hash["file_url"] = pal_candi.file_url
#
#           puts "The vote type name after:::::#{vote_type_name}"
#           puts "The const name after:::::#{const_name}"
#
#           _arr_candidates << _candidates_hash
#
#          end
#
#         puts "=========================#{_pal_candi.inspect}======================="
#
#         _final_resp = {
#             "resp_code" => "000",
#             "resp_desc" => "Success",
#             "message" => _arr_candidates,
#         }
#         MultiJson.dump(_final_resp, :pretty => true)
#       # end
#     end
#   elsif vote_master.id == 2
#     # if check_result_presi_existence.exists?
#     #   puts "*************************************************************"
#     #   puts "*************************************************************"
#     #   puts "*********************You have already done presidential***********************"
#     #   puts "*************************************************************"
#     #   puts "*************************************************************"
#     #
#     #   _exist = {
#     #       "resp_code" => "025",
#     #       "resp_desc" => "Exists",
#     #       "message" => "You has uploaded presidential results"
#     #   }
#     #   halt Oj.dump(_exist, :pretty => true)
#     # else
#       _pres_candi = CandidateMaster.where(vote_master_id: vote_type_id, constituency_master_id: _check_cand_id)
#       puts "=========================#{_pres_candi.inspect}======================="
#
#       _pres_candi.each do |pres_candi|
#         _candidates_hash = {}
#         _candidates_hash["candidate_name"] = pres_candi.name
#         _candidates_hash["candidate_id"] = pres_candi.id
#         _candidates_hash["vote_master_id"] = pres_candi.vote_master_id
#         _candidates_hash["candidate_position"] = pres_candi.position
#         _candidates_hash["constituency_master_id"] = _check_cand_id
#         _candidates_hash["party_master_id"] = pres_candi.party_master_id
#         _candidates_hash["file_url"] = pres_candi.file_url
#
#         # _candidates_hash["polling_station_name"] = _polling_station_info.name
#         # _candidates_hash["vote_master_id"] = booklet.vote_master_id
#
#         _vote_type_arr = _vote_master_info.where("vote_masters.id = #{pres_candi.vote_master_id}")
#         if _check_cand_id == nil?
#
#           _check_cand_id = nil
#           _const_master_arr = _constituency_master_info.where("constituency_masters.id = #{_check_cand_id}")
#
#           _const_master_arr.each do |const|
#             const_name = const.name
#           end
#         end
#         _party_master_arr = _party_master_info.where("party_masters.id = #{pres_candi.party_master_id}")
#
#
#         puts "The const name before:::::#{const_name}"
#
#         puts "The file path:::::#{pres_candi.file_url}"
#         _vote_type_arr.each do |vote|
#           vote_type_name = vote.name
#         end
#
#
#         _party_master_arr.each do |party|
#           party_name = party.name
#         end
#
#         _candidates_hash["vote_master_name"] = vote_type_name
#         _candidates_hash["constituency_name"] = const_name
#         _candidates_hash["party_name"] = party_name
#         # _candidates_hash["registed_voters"] = booklet.registed_voters
#         # _candidates_hash["booklet_number"] = booklet.booklet_number
#         # _candidates_hash["ballot_papers_total"] = booklet.ballot_papers_total
#
#         puts "The vote type name after:::::#{vote_type_name}"
#
#         _arr_candidates << _candidates_hash
#
#        end
#       _final_resp = {
#           "resp_code" => "000",
#           "resp_desc" => "Success",
#           "message" => _arr_candidates,
#       }
#       MultiJson.dump(_final_resp, :pretty => true)
#     # end
#   end
#
# end

def addElectionResult
  request.body.rewind
  payload = request.body.read

  puts "*************This Payload Received #{payload.inspect}:::: #{params["results_array"]} ********************************************"

  _arr = JSON.parse(params["results_array"])
  _polling_station_id = params["polling_station_id"]
  _vote_master_id = params["vote_master_id"]
  _user_id = params["user_id"]
  valid = params["ps_valid_votes"]
  rejected = params["ps_rejected_votes"]


  check_result_pal_existence = RecordsMaster.where(polling_station_id: _polling_station_id, user_id: _user_id, parliamentary_vote_status: true, presidential_vote_status: false)
  check_result_presi_existence = RecordsMaster.where(polling_station_id: _polling_station_id, user_id: _user_id, parliamentary_vote_status: false, presidential_vote_status: true)

  vote_master = VoteMaster.where(id: _vote_master_id).first

  puts "*********** Vote Master #{vote_master.inspect} #{vote_master.id} ***************"
  puts "*********** Pal Record #{check_result_pal_existence.inspect} ***************"
  puts "*********** Pres Record #{check_result_presi_existence.inspect} ***************"


  if vote_master.id == 1

    if check_result_pal_existence.exists?
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "********                                          ***********"
      puts "********                                          ***********"
      puts "********    You have already done parliamentary   ***********"
      puts "********                                          ***********"
      puts "********                                          ***********"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"

      _parl_exist_hash = {
          "resp_code" => "025",
          "resp_desc" => "Result exists",
          "message" => "Parliamentary result uploaded already."
      }

      halt Oj.dump(_parl_exist_hash, :pretty => true);
    else

      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "********                                          ***********"
      puts "********                                          ***********"
      puts "********      INSERTING PARLIAMENTARY RESULTS     ***********"
      puts "********                                          ***********"
      puts "********                                          ***********"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      try_code = generate_pink_sheet_code
      _image_url = uploadComplaintImage(try_code)
      insertIntoPinkSheet(_polling_station_id, valid, rejected, _user_id, try_code, _image_url)

      _arr.each do |election|
        puts "******* Pink Sheet Cod:::: #{try_code} Candidate Name::::#{election["candidate_name"]},Candidate Votes::::#{election["cand_votes_obtained"]}********************************"


        _parliament_rec_insertion = RecordsMaster.create!(
            pink_sheet_master_id: try_code,
            polling_station_id: _polling_station_id,
            candidate_master_id: election["candidate_id"],
            votes_obtained: election["cand_votes_obtained"],
            user_id: _user_id,
            parliamentary_vote_status: true,
            presidential_vote_status: false
        )

        if _parliament_rec_insertion.id > 0
          puts "************************************************************************************************"
          puts "******************************* #{_parliament_rec_insertion.inspect} ***************************"
        end


      end

      _parl_success_hash = {
          "resp_code" => "022",
          "resp_desc" => "Success",
          "message" => "Parliamentary result successfully uploaded."
      }

      halt Oj.dump(_parl_success_hash, :pretty => true)
    end
  elsif vote_master.id == 2
    puts "your insertion here"

    if check_result_presi_existence.exists?
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "********                                          ***********"
      puts "********                                          ***********"
      puts "********    You have already done PRESIDENTIAL    ***********"
      puts "********                                          ***********"
      puts "********                                          ***********"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"

      _presi_exist_hash = {
          "resp_code" => "026",
          "resp_desc" => "Result exists",
          "message" => "Presidential result uploaded already."
      }

      halt Oj.dump(_presi_exist_hash, :pretty => true)
    else

      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "********                                          ***********"
      puts "********                                          ***********"
      puts "********      INSERTING PRESIDENTIAL RESULTS      ***********"
      puts "********                                          ***********"
      puts "********                                          ***********"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      puts "*************************************************************"
      try_code = generate_pink_sheet_code
      _image_url = uploadComplaintImage(try_code)
      insertIntoPinkSheet(_polling_station_id, valid, rejected, _user_id, try_code, _image_url)

      _arr.each do |election|
        puts "******* Pink Sheet Cod:::: #{try_code} Candidate Name::::#{election["candidate_name"]},Candidate Votes::::#{election["cand_votes_obtained"]}********************************"

        _president_rec_insertion = RecordsMaster.create!(
            pink_sheet_master_id: try_code,
            polling_station_id: _polling_station_id,
            candidate_master_id: election["candidate_id"],
            votes_obtained: election["cand_votes_obtained"],
            user_id: _user_id,
            parliamentary_vote_status: false,
            presidential_vote_status: true
        )

        if _president_rec_insertion.id > 0
          puts "**********************************************************************************************"
          puts "******************************* #{_president_rec_insertion.inspect} ***************************"
        end

      end

    end

    _presi_success_hash = {
        "resp_code" => "023",
        "resp_desc" => "Success",
        "message" => "Presidential result successfully uploaded."
    }

    halt Oj.dump(_presi_success_hash, :pretty => true)
  else
    puts "The else insertion"
    # try_code = generate_pink_sheet_code
    # puts "There is nothing here you can do your insertion here"
    #
    # insertIntoPinkSheet(_polling_station_id, valid, rejected, _user_id, try_code)
    #
    # _arr.each do |election|
    #   puts "******* Pink Sheet Cod:::: #{try_code} Candidate Name::::#{election["candidate_name"]},Candidate Votes::::#{election["cand_votes_obtained"]}********************************"
    #
    #   if vote_master.id == 1
    #     _parliament_rec_insertion = RecordsMaster.create!(
    #         pink_sheet_master_id: try_code,
    #         polling_station_id: _polling_station_id,
    #         candidate_master_id: election["candidate_id"],
    #         votes_obtained: valid,
    #         user_id: _user_id,
    #         parliamentary_vote_status: true,
    #         presidential_vote_status: false
    #     )
    #
    #     if _parliament_rec_insertion.id > 0
    #       puts "************************************************************************************************"
    #       puts "******************************* #{_parliament_rec_insertion.inspect} ***************************"
    #     end
    #
    #   elsif vote_master.id == 2
    #     _president_rec_insertion = RecordsMaster.create!(
    #         pink_sheet_master_id: try_code,
    #         polling_station_id: _polling_station_id,
    #         candidate_master_id: election["candidate_id"],
    #         votes_obtained: valid,
    #         user_id: _user_id,
    #         parliamentary_vote_status: false,
    #         presidential_vote_status: true
    #     )
    #
    #     if _president_rec_insertion.id > 0
    #       puts "**********************************************************************************************"
    #       puts "******************************* #{_president_rec_insertion.inspect} ***************************"
    #     end
    #
    #   end
  end


  _already_exist = {
      "resp_code" => "021",
      "resp_desc" => "Failure",
      "message" => "PAYLOAD has been received"
  }


  Oj.dump(_already_exist, :pretty => true)
end

def uploadComplaintImage(engaged_code)
  _image_path = ""
  _filename = params[:file][:filename]
  if _filename.length < 1
    _image_path = ""
  else
    _filename = "#{engaged_code}_" + _filename
    file = params[:file][:tempfile]
    File.open("./public/#{_filename}", 'wb') do |f|
      f.write(file.read)
    end

    #checkImageExistence(imageURL)

    _image_path = "/#{_filename}"
  end

  if checkImageExistence(_image_path)
    _image_path = ""
  end

  puts "Image URL ::: #{_image_path}"
  _image_path
end

def checkImageExistence(imageURL)
  PinkSheetMaster.where(image_path: imageURL).exists?
end

def sendEmailToUser
  Pony.mail({
                :to => "slawer31@gmail.com",
                :from => 'slawer@st.vvu.edu.gh',
                :body => "Passed with flying colours",
                # :attachments => {"dalex_ussd.csv" => File.read("/opt/albert/test_scripts/dalex_ussd.csv")},
                :via_options => {
                    :address => 'localhost',
                    :port => 25
                },
                :subject => "Testing Email Feature"
            })

  # Pony.options = {
  #     :to => "slawer31@gmail.com",
  #     # :from => "slawer@st.vvu.edu.gh",
  #     :subject => "Some Subject",
  #     :body => "This is the body.",
  #     :via => :smtp,
  #     :via_options => {
  #         :address              => 'smtp.gmail.com',
  #         :port                 => '587',
  #         :enable_starttls_auto => true,
  #         :user_name            => 'slawer@st.vvu.edu.gh',
  #         :password             => 'disFARplay',
  #         :authentication       => :plain, # :plain, :login, :cram_md5, no auth by default
  #         :domain               => "sudotech.localdomain"
  #     }
  # }

end

# def send_email
#   puts "**************************************************************"
#   puts "**************************************************************"
#   puts "**************************************************************"
#   demo = Pony.mail({
#                      :to => 'slawer@st.vvu.edu.gh',
#                      # :from => 'slawer31@gmail.com',
#                      :from => 'no-reply@sudotech.com',
#                      :subject => 'Heya',
#                      :body => 'It worked',
#                      # :attachments => {"dalex_ussd.csv" => File.read("/opt/albert/test_scripts/dalex_ussd.csv")},
#                      :via => :smtp,
#                      :via_options => {
#                          :address => 'smtp.gmail.com',
#                          :port => '587',
#                          :enable_starttls_auto => true,
#                          :user_name => 'slawer31@gmail.com',
#                          :password => '',
#                          :authentication => :plain, # :plain, :login, :cram_md5, no auth by default
#                          :domain => "localhost.localdomain", # the HELO domain provided by the client to the server
#                      }
#                  })
#
#   # return ERR_EMAIL_SUCCESS.to_json
#   #
#   puts "**************************************************************"
#   puts "**************************************************************"
#   puts "**************************************************************"
#   puts "#{demo.inspect}"
#   puts "**************************************************************"
#   puts "**************************************************************"
#   puts "**************************************************************"
# end