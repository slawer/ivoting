STR_SIGN_UP_PASS = {'resp_desc' => "Success", 'resp_code' => "000", 'message' => "Sign up successful!"}
ERR_SIGN_UP_FAILED = {'resp_desc' => "Failure", 'resp_code' => "001", 'message' => "Sign up failed.User already exist!"}
ERR_EXIST = {'resp_desc' => "Failure", 'resp_code' => "002", 'message' => "User account not available"}
ERR_SIGN_IN = {'resp_desc' => "Failure", 'resp_code' => "007", 'message' => "Invalid username or password"}
ERR_USR_AVAIL = {'resp_desc' => "Failure", 'resp_code' => "008", 'message' => "User with same credentials exist"}
STR_SIGN_IN_PASS = {'resp_desc' => "Success", 'resp_code' => "000", 'message' => "Sign In successful!"}
ERR_SIGN_UP = {'resp_desc' => "Failure", 'resp_code' => "009", 'message' => "There was a problem signing you up.Please try again later"}
ERR_NON_CAND = {'resp_desc' => "Failure", 'resp_code' => "010", 'message' => "You are not allowed to use this service"}
STR_UPDATE_PASS = {'resp_code' => "000", 'resp_desc' => "Bio Data updated successfully."}
STR_UPDATE_FAIL = {'resp_code' => "010", 'resp_desc' => "There was a problem updating Bio Data.Please try again later."}
STR_UPDATE_STATUS_FOUND = {'resp_code' => "011", 'resp_desc' => "Sorry there's a pending Bio Data update. Please try again later."}
ERR_NO_ALLERGIES = {'resp_code' => '011', 'resp_desc' => 'No Allergies Available'}
ERR_NO_CONDITIONS = {'resp_code' => '012', 'resp_desc' => 'No Conditions Available'}
ERR_NO_UTYPE = {'resp_code' => '012', 'resp_desc' => 'No Usertype Available'}
ERR_NO_JOBS = {'resp_code' => '012', 'resp_desc' => 'No Jobs Available'}
ESCALATION_SUCC = {'resp_code' => '013', 'resp_desc' => 'Escalation sent successfully.'}
ESCALATION_FAIL = {'resp_code' => '014', 'resp_desc' => 'Escalation processing failed. Please try again later.'}
ESC_TPYE = "ESC"
BDT_TPYE = "BDT"