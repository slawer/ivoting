def createAppUser(last_name, other_names, email, contact, password_hash, password_salt)
  _data = User.create!(
      last_name: last_name,
      other_names: other_names,
      email: email,
      contact_number: contact,
      #user_name: userName,
      polling_station_id: 1,
      password_hash: password_hash,
      password_salt: password_salt,
  )

  puts "----Insert Data-----"
  puts _data.inspect

  if _data.id > 0
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "********                                          ***********"
    puts "********       USER REGISTRATION SUCCESSFUL       ***********"
    puts "********          #{STR_SIGN_UP_PASS.inspect}     ***********"
    puts "********                                          ***********"
    puts "********                                          ***********"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    puts "*************************************************************"
    Oj.dump(STR_SIGN_UP_PASS, :pretty => true)
  else
    halt Oj.dump(ERR_SIGN_UP, :pretty => true)
  end

end

def createAppOffsiteUser(firstName, lastName, otherName, email, phoneNumber, dob, gender, userType, password_hash, password_salt)
  _data = User.create!(
      first_name: firstName,
      last_name: lastName,
      other_name: otherName,
      phone_number: phoneNumber,
      email: email,
      dob: dob,
      gender: gender,
      password_hash: password_hash,
      password_salt: password_salt,
      user_type_id: userType,
      status: true,
      delete_status: false,
  )

  puts "----Insert Data-----"
  puts _data.inspect

  if _data.id > 0

  else
    halt Oj.dump(ERR_SIGN_UP)
  end

end


def addOffsiteUserInfo(firstName, lastName, otherName, dob, gender, record_type_id, phoneNumber, email)

  get_person_id = generate_person_code

  _data = PersonInfo.create!(
      person_code: get_person_id,
      surname_name: lastName,
      other_names: otherName,
      first_name: firstName,
      dob: dob,
      gender: gender,
      record_type_id: record_type_id,
      status1: true,
      status2: false
  )

  _person_contact = ContactInfo.create!(
      person_id: get_person_id,
      contact_number: phoneNumber,
      contact_email: email,
      status1: true,
      status2: false
  )

  puts "----Insert Data-----"
  puts "--------------------"
  puts _data.inspect
  puts "--------------------"
  puts "----Insert Data-----"


  puts "----Person Contact Data-----"
  puts "--------------------"
  puts _person_contact.inspect
  puts "--------------------"
  puts "----Person Contact Data-----"

  if _data.id > 0
  elsif _person_contact > 0

  else
    halt Oj.dump(ERR_SIGN_UP)
  end

end

def createBooklet(user_id, polling_station_id, vote_master_id, registed_voters, booklet_number, ballot_papers_total)

  _booklet_master_hash = BookletMaster.create!(
      user_id: user_id,
      polling_station_master_id: polling_station_id,
      vote_master_id: vote_master_id,
      registed_voters: registed_voters,
      booklet_number: booklet_number,
      ballot_papers_total: ballot_papers_total
  )

  if _booklet_master_hash.id > 0
    _created_hash = {
        "resp_code" => "011",
        "resp_desc" => "Success",
        "message" => "Successfully Saved"
    }
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>> #{_booklet_master_hash.inspect}>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>> #{_created_hash.inspect}>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    Oj.dump(_created_hash)
  else
  end
end

# def checkUserExistence(email)
#   return User.where("email = '#{email}'").exists?
#
# end

def generate_pink_sheet_code

  sql = "select nextval('pink_sheet_masters_id_seq')"
  val = ActiveRecord::Base.connection.execute(sql)

  rem = val.values[0][0]
  code = "%09d" % rem
  val = "PNK" << code
  puts "For Prospect ID, #{val}"
  val
end

def insertIntoPinkSheet (polling_station_id, valid_votes, rejected_votes, user_id, pinksheet_unique_id, _image_url)

  _rec_insertion = PinkSheetMaster.create!(
      polling_station_id: polling_station_id,
      valid_votes: valid_votes,
      rejected_votes: rejected_votes,
      user_id: user_id,
      pinksheet_unique_id: pinksheet_unique_id,
      image_path: _image_url
  )


    puts "************ PinkSheet Info :::: #{_rec_insertion.inspect} ****************"


end