ActiveRecord::Base.establish_connection(
    adapter: 'postgresql',
    host: 'localhost',
    username: 'sylvanus',
    password: 'sRaDeNaM1',
    database: 'voting_db'
)

class User < ActiveRecord::Base
  self.table_name = "users"
end

class AppUser < ActiveRecord::Base
  self.table_name = "app_users"
end

class PollingStationMaster < ActiveRecord::Base
  self.table_name = "polling_station_masters"
end

class VoteMaster < ActiveRecord::Base
  self.table_name = "vote_masters"
end

class BookletMaster < ActiveRecord::Base
  self.table_name = "booklet_masters"
end

class CandidateMaster < ActiveRecord::Base
  self.table_name = "candidate_masters"
end

class ConstituencyMaster < ActiveRecord::Base
  self.table_name = "constituency_masters"
end

class PartyMaster < ActiveRecord::Base
  self.table_name = "party_masters"
end

class RecordsMaster < ActiveRecord::Base
  self.table_name = "records_masters"
end

class PinkSheetMaster < ActiveRecord::Base
  self.table_name = "pink_sheet_masters"
  self.primary_key = "pinksheet_unique_id"
  #belongs_to :a_station, class_name: "PollingStationMaster", foreign_key: :polling_station_id
end