require 'sinatra'
require 'rubygems'
require 'active_record'
require 'active_support'
require 'bcrypt'
require 'json'
require 'multi_json'
require 'oj'
require 'puma'
require 'pony'
require 'paperclip'
require 'net/http'
require 'faraday'
require 'base64'
require 'uri'
require './model/db_config'
require './db_routines'
require './constants'
require './functions'

# #set :bind => '192.168.137.1'
# set :bind => '192.168.10.61'
# #set :bind => '192.168.43.192'
# set :port => '7079'

set :public_folder, File.dirname(__FILE__)


post "/login" do
  login
end

post "/sign_up" do
  createAccount
end


get "/get_voter_type" do
  getVoteMaster
end

post "/add_booklet_details" do
  addBookletDets
end

post "/booklet_inventory" do
  getVoteInventory
end

post "/result_inventory" do
  getVotingResults
end

post "/get_candidates" do
  getCandidates
end

post "/send_election_result" do
  addElectionResult
end

post "/send_email" do
  #send_email
end